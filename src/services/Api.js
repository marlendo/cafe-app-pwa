import axios from 'axios';

// Address
// const Host = 'http://192.168.0.102';
// const Host = 'http://localhost:4000';
const Host = 'https://cafe-bosku-api.herokuapp.com';

// Main URL
const appServer = Host;

// Interface
const Menu = 'menu';

// const isIE = /*@cc_on!@*/false || !!document.documentMode;

const header = {
  'Content-Type': 'application/json'
}

export const cafeApi =
  axios.create({
    baseURL: appServer,
    header,
    timeout: 40000
  })