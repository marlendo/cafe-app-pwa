import SecureStorage from 'secure-web-storage';
import CryptoJS from 'crypto-js';
import { Naruto, Hinata } from './SafeLoad';

export const secureStorage = new SecureStorage(localStorage, {
  hash: function hash(key) {
    key = CryptoJS.SHA256(key, Naruto);

    return key.toString();
  },
  encrypt: function encrypt(data) {
    data = CryptoJS.AES.encrypt(data, Naruto);

    data = data.toString();

    return data;
  },
  decrypt: function decrypt(data) {
    data = CryptoJS.AES.decrypt(data, Naruto);

    data = data.toString(CryptoJS.enc.Utf8);

    return data;
  }
})

export const secureSession = new SecureStorage(sessionStorage, {
  hash: function hash(key) {
    key = CryptoJS.SHA256(key, Naruto);

    return key.toString();
  },
  encrypt: function encrypt(data) {
    data = CryptoJS.AES.encrypt(data, Naruto);

    data = data.toString();

    return data;
  },
  decrypt: function decrypt(data) {
    data = CryptoJS.AES.decrypt(data, Naruto);

    data = data.toString(CryptoJS.enc.Utf8);

    return data;
  }
})

export const aesEncrypt = (pass) => {
  var ciphertext = CryptoJS.AES.encrypt(pass, Hinata);
  return ciphertext.toString();
}
