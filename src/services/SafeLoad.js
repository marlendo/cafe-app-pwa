export const Naruto = 'erickganteng123';
export const Hinata = 'erickmauapaajaah';
export const SaveLoad = (session) => {
    console.log(session)
    var failed = { status: false, data: session };
    var done = { status: true, data: session };

    function ceckTime() {
        let currentDate = new Date().getTime();
        let date = localStorage.getItem('time');
        if (date === null) {
            return true
        } else {
            if (currentDate - Number(date) < 3600000) {
                return true
            } else {
                return false
            }
        }
    }

    if (session !== null) {
        if (session.role === 'ADMIN') {
            if (ceckTime()) {
                localStorage.setItem('time', new Date().getTime());
                return done;
            } else {
                localStorage.clear();
                return failed;
            }
        } else if (session.role === 'USER') {
            localStorage.setItem('time', new Date().getTime());
            return done;
        } else {
            return failed;
        }
    } else {
        return failed;
    }
}