import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Main from './app/Main';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.render(
    <BrowserRouter>
        {/* <BrowserRouter basename='/cafe-bosku'> */}
        <Main />
    </BrowserRouter>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
if (process.env.NODE_ENV === 'development') {
    serviceWorker.unregister()
} else {
    serviceWorker.register()
}
