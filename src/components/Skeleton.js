import React from 'react';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
// import PropTypes from 'prop-types';
// import clsx from 'clsx';
// import { makeStyles } from '@material-ui/core/styles';
// import CheckCircleIcon from '@material-ui/icons/CheckCircle';
// import ErrorIcon from '@material-ui/icons/Error';
// import InfoIcon from '@material-ui/icons/Info';
// import CloseIcon from '@material-ui/icons/Close';
// import { amber, green } from '@material-ui/core/colors';
// import IconButton from '@material-ui/core/IconButton';
// import Snackbar from '@material-ui/core/Snackbar';
// import SnackbarContent from '@material-ui/core/SnackbarContent';
// import WarningIcon from '@material-ui/icons/Warning';
// import Button from '@material-ui/core/Button';

export const SkeletonElement = (state) => {

    const count = state.count;
    const color = state.color;
    const highlight = state.highlight;
    const width = state.width;

    // const [snack, setSnack] = useState({ overide: false, open: true });

    return (
        <SkeletonTheme color={color} highlightColor={highlight}>
            <p className={'title-menu zero'} style={{ width: width }}>
                <Skeleton count={count} />
            </p>
        </SkeletonTheme>
    )
}