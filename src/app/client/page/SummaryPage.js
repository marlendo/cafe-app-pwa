import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
//icon
import sukron from '../../../assets/sukron.gif';
//component
import { cafeApi } from '../../../services/Api';
import { secureStorage } from '../../../services/SecureStorage';

function SummaryPage(props) {

  const [menuOrder, setMenuOrder] = useState([])

  function transaksi() {
    let payload = { id_user: secureStorage.getItem('session').id_user, total_keseluruhan: 0, payload: [] };
    for (let i in menuOrder) {
      payload.total_keseluruhan = payload.total_keseluruhan + menuOrder[i].harga * menuOrder[i].count;
      payload.payload.push({ id_menu: menuOrder[i].id_menu, jumlah_pembelian: menuOrder[i].count, harga: menuOrder[i].harga, jumlah_harga: menuOrder[i].harga * menuOrder[i].count });
    }
    // console.log(payload)
    cafeApi.post('/transaksi', payload)
      .then(res => {
        console.log(res.data)
        props.history.push({
          pathname: '/'
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

  useEffect(() => {    
    setMenuOrder(props.history.location.state.data);
  }, []);

  return (
    <div className={'padding-small primary-bg'}>
      <div className='space-lg' />
      <p className='title-cafe zero' style={{ fontSize: '60px' }}>Terimakasih</p>
      <div className='space-lg' />
      <div className='space-lg' />
      <img src={sukron} alt='sukron-katshiron'
        style={{
          width: 'calc(100% - 40px)',
          borderRadius: '20px',
          marginLeft: '20px',
          boxShadow: '0 0 10px #000'
        }} />
      <div className='space-lg' />
      <div className='space-lg' />
      <div style={{ position: 'relative', width: '100%' }}>
        <Button
          variant='contained'
          size={'large'}
          color='primary'
          fullWidth
          onClick={() => {
            transaksi()
          }}
        >
          Sama - Sama
      </Button>
      </div>
    </div>
  );
}

export default withRouter(SummaryPage)