import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import { secureStorage } from '../../../services/SecureStorage';
// import {
//   BrowserView
// } from 'react-device-detect';
//icon
import Logo from '../../../assets/cafe-logo.png'
//components
import { SnackBarElement } from '../../../components/SnackBar';
import { cafeApi } from '../../../services/Api';

function LoginUser(props) {

  const [snack, setSnack] = useState({ open: false, variant: 'success', message: '', timeOut: 3000 });

  // const [payload, setPayload] = useState({ name: '', email: '' })

  useEffect(() => {
    console.log('Jalan')
  }, [])

  function doLogin(payload) {
    cafeApi.post('/user-login', payload)
      .then(res => {
        if (res.data.status === 200) {
          secureStorage.setItem('session', res.data.payload);
          props.history.push({
            pathname: '/',
            state: {}
          })
        } else {
          startSnackBar({ message: res.data.payload, variant: 'error' })
        }
      })
      .catch(err => {
        startSnackBar({ message: 'Login Failed', variant: 'error' })
      })
  }

  function startSnackBar(param) {
    setSnack({
      ...snack,
      open: true,
      message: param.message,
      variant: param.variant
    });
    setTimeout(function () {
      setSnack({
        ...snack,
        open: false,
        variant: param.variant
      });
    }, snack.timeOut);
  }

  const responseGoogle = (response) => {
    if (response.profileObj.email !== undefined) {
      doLogin({ name: response.profileObj.name, email: response.profileObj.email })
    }
  }

  return (
    <div className={'overide-container'}>
      {/* <BrowserView> */}
      {SnackBarElement(snack)}
      <div className={'main-container primary-bg'}>
        <div style={{
          width: '100%',
          height: '60vh',
          backgroundColor: '#000'
        }} />
        <div style={{
          position: 'fixed',
          width: '100%',
          top: '40vh'
        }}>
          <div className={'flex-center-row'}>
            <div style={{ padding: '10px', borderRadius: '100%' }} className={'primary-bg'}>
              <img src={Logo} alt='logo' className={'login-logo'} />
            </div>
          </div>
        </div>
      </div>
      {/* </BrowserView> */}
      <div className={'flex-center-row'}>
        <GoogleLogin
          className={'button-google'}
          clientId="879984144920-4n9hl3j7r175197kud14u5r503jcmpvu.apps.googleusercontent.com"
          buttonText="Login"
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
          cookiePolicy={'single_host_origin'}
        />
      </div>
    </div>
  );
}

export default withRouter(LoginUser)