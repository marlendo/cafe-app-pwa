import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import {
  BrowserView,
  MobileView
} from 'react-device-detect';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Fab from '@material-ui/core/Fab';
import Badge from '@material-ui/core/Badge';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import Typography from '@material-ui/core/Typography';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import FolderIcon from '@material-ui/icons/Folder';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
//icon
import Icoffe from '../../../assets/dummy/coffe.jpg';
import Igorengan from '../../../assets/dummy/gorengan.jpg';
import Logo from '../../../assets/cafe-logo.png';
import ShoppingCart from '@material-ui/icons/ShoppingCart';
import RemoveShoppingCart from '@material-ui/icons/RemoveShoppingCart';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddCircle from '@material-ui/icons/AddCircle';
import RemoveCircle from '@material-ui/icons/RemoveCircle';
//components
import { SnackBarElement } from '../../../components/SnackBar';
import { SkeletonElement } from '../../../components/Skeleton';
import { cafeApi } from '../../../services/Api';

function MenuPage(props) {

  const [menuCafe, setMenuCafe] = useState([])
  const [menuOrder, setMenuOrder] = useState([])
  const [snack, setSnack] = useState({ open: false, variant: 'success', message: '', timeOut: 3000 });
  const [value, setValue] = React.useState('recents');

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  function removeOrder(item) {
    item.order = false;
    let removeOrder = menuOrder.filter(function (v) {
      return v.id !== item.id
    });
    setMenuOrder(removeOrder)
  }

  function addOrder(index, childIndex) {
    let menu = [...menuCafe];
    let item = menu[index].child[childIndex];
    item.order = true;
    item.count = 1;
    setMenuCafe(menu);
    let dataOrder = menuOrder.filter(function (v) {
      return v.id_menu === item.id_menu
    });
    if (dataOrder.length < 1) {
      setMenuOrder([...menuOrder, item])
    }
  }

  function startSnackBar(param) {
    setSnack({
      ...snack,
      open: true,
      message: param.message,
      variant: param.variant
    });
    setTimeout(function () {
      setSnack({
        ...snack,
        open: false,
        variant: param.variant
      });
    }, snack.timeOut);
  }

  useEffect(() => {
    cafeApi.get('/menu')
      .then(res => {
        if (res.data.status === 200) {
          setMenuCafe(res.data.payload)
        } else {
          startSnackBar({ message: res.data.payload, variant: 'error' })
        }
      })
      .catch(err => {
        startSnackBar({ message: 'Get Menu Failed', variant: 'error' })
      })
  }, [])

  return (
    <div className={'padding-small primary-bg'}>
      {SnackBarElement(snack)}
      {/* <BrowserView> */}
        <Fab
          variant={'extended'}
          color="secondary"
          className={'button-card-cart button-corner-cart'}
          onClick={() => {
            if (menuOrder.length === 0) {
              startSnackBar({ message: 'pilih menu dulu bosku', variant: 'warning' })
            } else {
              props.history.push({
                pathname: 'pembayaran',
                state: { data: menuOrder }
              })
            }
          }}>
          <Badge badgeContent={menuOrder.length} color="primary">
            <ShoppingCart />
          </Badge>
          &nbsp; <b>Lanjut Kuy</b>
        </Fab>
      {/* </BrowserView> */}
      {menuCafe.length === 0 ? (
        <div style={{ margin: '0 10px' }}>
          <div className={'padding-md'} style={{ backgroundColor: '#3E2723' }}>
            {SkeletonElement({ count: 1, color: '#795548', highlight: '#FFFFFF', width: '200px' })}
          </div>
          <div className={'padding-md'} style={{ backgroundColor: '#3E2723' }}>
            {SkeletonElement({ count: 1, color: '#795548', highlight: '#FFFFFF', width: '200px' })}
          </div>
        </div>
      ) : (
          <div />
        )}
      {menuCafe.map((item, index) => (
        <ExpansionPanel style={{ margin: '0 10px', backgroundColor: 'transparent' }} key={index.toString()}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon style={{ color: '#FFF' }} />}
            style={{ backgroundColor: '#3E2723' }}
          >
            <p className={'title-menu zero'}>{item.judul}</p>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails
            className={'primary-bg'}
            style={{ borderBottomLeftRadius: '15px', borderBottomRightRadius: '15px', padding: 0 }}>
            <Grid container>
              {item.child.map((childItem, childIndex) => (
                <Grid item xs={12} sm={4} md={3} key={childIndex.toString()}>
                  <Card className={'card-menu'}>
                    {childItem.order ? (
                      <div className={'button-card-cart flex-center-row'}>
                        <Fab color="secondary" onClick={() => {
                          removeOrder(childItem)
                        }}>
                          <RemoveShoppingCart />
                        </Fab>
                      </div>
                    ) : (
                        <div />
                      )}
                    <CardActionArea>
                      <CardContent
                        onClick={() => {
                          addOrder(index, childIndex)
                        }}>
                        <img
                          className={'img-menu'}
                          src={childItem.gambar === 'default' ? Logo : childItem.gambar}
                          alt={'img-menu'}
                        />
                      </CardContent>
                      <CardContent>
                        <div className='prize'>
                          <p className='zero light'>Rp. {childItem.harga}</p>
                        </div>
                        {childItem.order ? (
                          <div className='prize-right'>
                            <div className='flex-center-row' style={{ backgroundColor: '#f50057', padding: '5px', marginLeft: '2px', borderRadius: '15px' }}>
                              <AddCircle
                                style={{ color: '#FFF' }}
                                onClick={() => {
                                  let state = [...menuCafe];
                                  state[index].child[childIndex] = { ...menuCafe[index].child[childIndex], count: childItem.count + 1 };
                                  setMenuCafe(state);
                                  menuOrder.filter(function (v) {
                                    return v.id_menu === childItem.id_menu
                                  })[0].count = childItem.count + 1;
                                }}
                              />
                              &nbsp;
                              <p className={'zero light'}>{childItem.count}</p>
                              &nbsp;
                                <RemoveCircle
                                style={{ color: '#FFF' }}
                                onClick={() => {
                                  let state = [...menuCafe];
                                  state[index].child[childIndex] = { ...menuCafe[index].child[childIndex], count: childItem.count - 1 <= 1 ? 1 : childItem.count - 1 };
                                  setMenuCafe(state);
                                  menuOrder.filter(function (v) {
                                    return v.id_menu === childItem.id_menu
                                  })[0].count = childItem.count - 1 <= 1 ? 1 : childItem.count - 1;
                                }}
                              />
                            </div>
                          </div>
                        ) : (
                            <div />
                          )}
                        <Typography gutterBottom variant="h5" component="h2" noWrap>
                          {childItem.title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p" noWrap>
                          {childItem.content}
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      ))}
      {/* <MobileView>
        <BottomNavigation
          value={value}
          onChange={handleChange}
          showLabels
          className={'button-bottom-nav'}
        >
          <BottomNavigationAction label="Recents" value="recents" icon={<RestoreIcon />} />
          <BottomNavigationAction label="Favorites" value="favorites" icon={<FavoriteIcon />} />
          <BottomNavigationAction label="Nearby" value="nearby" icon={<LocationOnIcon />} />
        </BottomNavigation>
      </MobileView> */}
    </div>
  );
}

export default withRouter(MenuPage)