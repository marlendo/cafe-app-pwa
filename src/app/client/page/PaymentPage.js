import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Button from '@material-ui/core/Button';
//icon
import Logo from '../../../assets/cafe-logo.png';
import Icoffe from '../../../assets/dummy/coffe.jpg';
import AddCircle from '@material-ui/icons/AddCircle';
import RemoveCircle from '@material-ui/icons/RemoveCircle';


function PaymentPage(props) {

  const [menuOrder, setMenuOrder] = useState([])
  const [total, setTotal] = useState(0)

  function totalPayment(data) {
    var calc = 0;
    for (let i in data) {
      calc += data[i].count * data[i].harga
    }
    setTotal(calc);
  }

  useEffect(() => {
    setMenuOrder(props.history.location.state.data);
    totalPayment(props.history.location.state.data);
  }, []);

  return (
    <div className={'padding-small primary-bg'}>
      <List>
        {menuOrder.map((item, index) => (
          <div key={index.toString()}>
            <ListItem alignItems="center" style={{ backgroundColor: '#FFFFFF', margin: '5px 0' }}>
              <ListItemAvatar>
                <img
                  style={{ width: '70px', height: '70px', objectFit: 'cover', padding: '7px 20px 7px 0' }}
                  src={item.gambar === 'default' ? Logo : item.gambar}
                  alt={'img-order'}
                />
              </ListItemAvatar>
              <ListItemText
                primary={item.title}
                secondary={'Rp. ' + item.harga * item.count}
              />
              {/* <div className='flex-center-row' style={{ backgroundColor: '#f50057', padding: '5px 20px', marginRight: '10px', borderRadius: '15px' }}>
                <p className={'zero light'}>Rp. {item.harga * item.count}</p>
              </div> */}
              <div className='flex-center-row' style={{ backgroundColor: '#f50057', padding: '5px', marginLeft: '2px', borderRadius: '15px' }}>
                <AddCircle
                  style={{ color: '#FFF' }}
                  onClick={() => {
                    let state = [...menuOrder];
                    state[index] = { ...menuOrder[index], count: item.count + 1 };
                    setMenuOrder(state);
                    totalPayment(state);
                  }}
                />
                &nbsp;
                <p className={'zero light'}>{item.count}</p>
                &nbsp;
                <RemoveCircle
                  style={{ color: '#FFF' }}
                  onClick={() => {
                    let state = [...menuOrder];
                    state[index] = { ...menuOrder[index], count: item.count - 1 <= 1 ? 1 : item.count - 1 };
                    setMenuOrder(state);
                    totalPayment(state);
                  }}
                />
              </div>
            </ListItem>
          </div>
        ))}
        <ListItem alignItems="center" style={{ backgroundColor: '#f50057', margin: '5px 0' }}>
          <ListItemText>
            <p className='zero light'>Total Harga</p>
          </ListItemText>
          <div className='flex-center-row' style={{ backgroundColor: '#FFF', padding: '5px 10px', borderRadius: '15px', minWidth: '190px' }}>
            <p className={'zero secondary'}>Rp. {total}</p>
          </div>
        </ListItem>
      </List>
      <div className='space' />
      <div className='space' />
      <div className='space' />
      <div style={{ position: 'fixed', bottom: 0, width: '100%', marginLeft: '-5px' }}>
        <Button
          variant='contained'
          size={'large'}
          color='primary'
          fullWidth
          onClick={() => {
            props.history.push({
              pathname: 'end',
              state: { data: menuOrder }
            })
          }}
        >
          Oke Bosku
      </Button>
      </div>
    </div>
  );
}

export default withRouter(PaymentPage)