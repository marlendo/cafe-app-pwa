import React, { useState, useEffect } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
//icon
import CafeLogo from '../../assets/cafe-logo.png';
//page
import MenuPage from './page/MenuPage';
import LoginUser from './page/LoginUser';
import PaymentPage from './page/PaymentPage';
import SummaryPage from './page/SummaryPage';
//component
import { cafeApi } from '../../services/Api';
import { SaveLoad } from '../../services/SafeLoad';
import { secureStorage } from '../../services/SecureStorage';

function MainMobile(props) {

  function pengunjung() {
    cafeApi.post('/pengunjung', {})
      .then(res => {
        console.log(res)
      })
      .catch(err => {
        console.log(err)
      })
  }


  function ceckUserLogin() {
    if (SaveLoad(secureStorage.getItem('session')).status) {
      pengunjung()
    } else {
      props.history.push({
        pathname: '/user',
        state: {}
      })
    }
  }

  useEffect(() => {
    ceckUserLogin();
  }, [])

  return (
    <Container className={'main-container primary-bg zero'}>
      <div className='flex-center-row end' style={{
        width: 'calc(100% - 140px)',
        height: '5px',
        background: '#000',
        position: 'absolute',
        top: '60px',
        marginLeft: '140px'
      }}>
        <p className={'subtitle-cafe'}> Tempat Nongkrong Sadulur </p>
      </div>
      <div className='flex-center-column' style={{
        width: 'calc(100% - 140px)',
        height: '50px',
        background: '#3E2723',
        position: 'absolute',
        top: '70px',
        marginLeft: '140px'
      }}>
        <p className={'title-cafe zero'}>{props.location.pathname === '/pembayaran' ? 'Pembayaran Bosku' : props.location.pathname === '/end' ? 'Pesanan Selesai' : props.location.pathname === '/user' ? 'Login Bentar Ya Bosku' : 'Daftar Menu Kedai Bosku'}</p>
      </div>
      <div className={'margin'} style={{
        position: 'absolute'
      }}>
        <img src={CafeLogo}
          style={{
            width: 'auto',
            height: '140px',
            borderRadius: '15px',
            border: '5px solid #3E2723'
          }} />
      </div>
      <div style={{
        position: 'absolute',
        width: '100%',
        top: '160px'
      }}>
        <Switch>
          <Route exact path="/" component={MenuPage} />
          <Route path="/user" component={LoginUser} />
          <Route path="/menu" component={MenuPage} />
          <Route path="/pembayaran" component={PaymentPage} />
          <Route path="/end" component={SummaryPage} />
          {/* if page not found */}
          {/* <Route component={PaymentPage} />       */}
        </Switch>
      </div>
    </Container>
  );
}

export default withRouter(MainMobile)