import React from 'react';
import { Switch, Route } from 'react-router-dom';
import '../styles/App.css';
import {
  BrowserView,
  MobileView
} from 'react-device-detect';

//page
import MainMobile from './client';
import MainWeb from './web';
import LoginUser from './client/page/LoginUser';
import LoginPage from './web/page/LoginPage';

function Main() {
  return (
    <div>
      <BrowserView>
        <Switch>          
          <Route exact path="/" component={MainMobile} />
          <Route path="/menu" component={MainMobile} />
          <Route path="/pembayaran" component={MainMobile} />
          <Route path="/end" component={MainMobile} />
          <Route path="/dashboard" component={MainWeb} />
          <Route path="/login" component={LoginPage} />
          <Route path="/user" component={LoginUser} />
        </Switch>
      </BrowserView>
      <MobileView>
        <Switch>
          <Route component={MainMobile} />
        </Switch>
      </MobileView>
    </div>
  );
}

export default Main;
