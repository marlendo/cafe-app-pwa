import React, { useState, useEffect } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Fab from '@material-ui/core/Fab';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
//icon
import Dashboard from '@material-ui/icons/Dashboard';
import ExitToApp from '@material-ui/icons/ExitToApp';
import RestaurantMenu from '@material-ui/icons/RestaurantMenu';
import Settings from '@material-ui/icons/Settings';
// page
import DashboardPage from './page/Dashboard';
import MenuPage from './page/MenuPage';
import SettingsPage from './page/SettingsPage';
//components
import { SaveLoad } from '../../services/SafeLoad';
import { secureStorage } from '../../services/SecureStorage';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const mainMenu = [
  { name: 'Dashboard', icon: <Dashboard />, link: '/dashboard' },
  { name: 'Daftar Menu', icon: <RestaurantMenu />, link: '/dashboard/menu' }
]

const secondaryMenu = [
  { name: 'Settings', icon: <Settings />, link: '/dashboard/settings' }
]

function MainWeb(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = useState(false);
  const [session, setSession] = useState({ user: '' });
  const menuName = props.history.location.pathname.split('/')[Number(props.history.location.pathname.split('/').length) - 1];

  function handleDrawerOpen() {
    setOpen(true);
  }

  function handleDrawerClose() {
    setOpen(false);
  }

  function ceckUserLogin() {
    if (SaveLoad(secureStorage.getItem('session')).status) {
      if (secureStorage.getItem('session').role === 'ADMIN') {
        setSession(secureStorage.getItem('session'))
      } else {
        props.history.push({
          pathname: '/login',
          state: {}
        })
      }
    } else {
      props.history.push({
        pathname: '/login',
        state: {}
      })
    }
  }

  useEffect(() => {
    ceckUserLogin();
  }, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <p className='zero title-menu'>{open ? '' : menuName}</p>
          <div style={{ flexGrow: 1 }} />
          <Avatar className={'light-bg secondary'}>{
            session.user.split(' ')[0].substring(0, 1)
          }</Avatar>
          <p className='zero title-menu'>&nbsp; Welcome {session.user}</p>
          <Button color='inherit' size='small' variant='text' onClick={() => {
            localStorage.clear();
            props.history.push({
              pathname: '/login',
              state: {}
            });
          }}>
            <ExitToApp />
            {/* &nbsp; Log Out */}
          </Button>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
        open={open}
      >
        <div className={classes.toolbar}>
          <p className='zero title-menu blue' style={{ textAlign: 'center', width: '100%' }}>{menuName}</p>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>
          {mainMenu.map((menu, index) => (
            <ListItem button key={menu.name} onClick={() => {
              props.history.push({
                pathname: menu.link,
                state: {}
              });
              // handleDrawerClose()
            }}>
              <ListItemIcon>{menu.icon}</ListItemIcon>
              <ListItemText primary={menu.name} />
            </ListItem>
          ))}
        </List>
        <Divider />
        <List>
          {secondaryMenu.map((menu, index) => (
            <ListItem button key={menu.name} onClick={() => {
              props.history.push({
                pathname: menu.link,
                state: {}
              });
              // handleDrawerClose();
            }}>
              <ListItemIcon>{menu.icon}</ListItemIcon>
              <ListItemText primary={menu.name} />
            </ListItem>
          ))}
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Switch>
          <Route exact path="/dashboard" component={DashboardPage} />
          <Route path="/dashboard/menu" component={MenuPage} />
          <Route path="/dashboard/settings" component={SettingsPage} />
        </Switch>
      </main>
    </div>
  );
}

export default withRouter(MainWeb)