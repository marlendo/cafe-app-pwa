import React, { useState, useEffect } from 'react';
import { ResponsiveContainer, AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts'
import { withRouter } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
//icon
//components
import { cafeApi } from '../../../services/Api';

function Dashboard(props) {

  const initialAnalitikPenjualan = [
    { name: 'Januari', penjualan: 0 },
    { name: 'Februari', penjualan: 0 },
    { name: 'Maret', penjualan: 0 },
    { name: 'April', penjualan: 0 },
    { name: 'Mei', penjualan: 0 },
    { name: 'Juni', penjualan: 0 },
    { name: 'Agustus', penjualan: 0 },
    { name: 'September', penjualan: 0 },
    { name: 'Oktober', penjualan: 0 },
    { name: 'November', penjualan: 0 },
    { name: 'Desember', penjualan: 0 }
  ];

  const [pengunjung, setPengunjung] = useState({ visitor: 0, allVisitor: 0 })
  const [penjualan, setPenjualan] = useState({ sale: 0, allSale: 0, info: 'Loading ...' })
  const [analitikPenjualan, setAnalitikPenjualan] = useState(initialAnalitikPenjualan)

  function calculatePengunjung(data) {
    let count = 0;
    for (let i in data) {
      count += data[i].jumlah_pengunjung
      if (data.length - 1 === Number(i)) {
        setPengunjung({
          ...pengunjung,
          visitor: data[i].jumlah_pengunjung,
          allVisitor: count
        });
        break;
      }
    }
  }

  function Analitik(data) {
    let thisDate = new Date().getDate();
    let count = 0;
    let date, month;
    let analitik = [...analitikPenjualan];
    for (let i in data) {
      count += data[i].jumlah_kalkulasi
      date = new Date(Number(data[i].date_kalkulasi))
      month = date.getMonth()
      analitik[month - 1].penjualan = analitik[month - 1].penjualan + data[i].jumlah_kalkulasi
      if (data.length - 1 === Number(i)) {
        console.log(thisDate - date.getDate())
        setPenjualan({
          ...pengunjung,
          sale: data[i].jumlah_kalkulasi,
          allSale: count,
          info: thisDate - date.getDate() === 0 ? 'Penjualan Hari Ini' : thisDate - date.getDate() === 1 ? 'Penjualan Kemarin' : thisDate - date.getDate() === 2 ? 'Penjualan Kemarin Lusa' : 'Penjualan Tanggal ' + date.getDate()
        });
        break;
      }
    }
    console.log(analitik)
    setAnalitikPenjualan(analitik)
  }

  function getPengunjung() {
    cafeApi.get('/pengunjung')
      .then(res => {
        calculatePengunjung(res.data.payload)
      })
      .catch(err => {
        console.log(err)
      })
  }

  function getKalkulasi() {
    cafeApi.get('/kalkulasi')
      .then(res => {
        Analitik(res.data.payload)
      })
      .catch(err => {
        console.log(err)
      })
  }

  useEffect(() => {
    getPengunjung()
    getKalkulasi()
  }, [])

  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <Paper className='paper-analityc secondary-bg' onClick={()=> {console.log(analitikPenjualan)}}>
            <p className='zero center light'>Pengunjung Hari ini</p>
            <div className='flex-center-column' style={{ height: 'calc(100% - 20px)' }}>
              <h1 className='light zero'>{pengunjung.visitor}</h1>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className='paper-analityc secondary-bg'>
            <p className='zero center light'>Total Pengunjung</p>
            <div className='flex-center-column' style={{ height: 'calc(100% - 20px)' }}>
              <h1 className='light zero'>{pengunjung.allVisitor}</h1>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className='paper-analityc secondary-bg'>
            <p className='zero center light'>{penjualan.info}</p>
            <div className='flex-center-column' style={{ height: 'calc(100% - 20px)' }}>
              <h1 className='light zero'>{penjualan.sale}</h1>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className='paper-analityc secondary-bg'>
            <p className='zero center light'>Total Penjualan</p>
            <div className='flex-center-column' style={{ height: 'calc(100% - 20px)' }}>
              <h1 className='light zero'>{penjualan.allSale}</h1>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} id='chart-container'>
          <Paper className='light-bg padding-lg'>
            <p className='title-menu zero dark'>Analitik Penjualan <b>Kedai Bosku</b> Tahun 2019 </p>
            <div style={{ width: '100%', height: 400 }}>
              <ResponsiveContainer>
                <AreaChart
                  data={analitikPenjualan}
                  margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="name" />
                  <YAxis />
                  <Tooltip />
                  <Area
                    type='monotone'
                    dataKey='penjualan'
                    stroke='#3f51b5'
                    fill='#3f51b5' />
                </AreaChart>
              </ResponsiveContainer>
            </div>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

export default withRouter(Dashboard)