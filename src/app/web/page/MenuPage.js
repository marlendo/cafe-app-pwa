import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import FileBase64 from 'react-file-base64';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
//icon
import logo from '../../../assets/cafe-logo.png';
import LibraryAdd from '@material-ui/icons/LibraryAdd';
import Edit from '@material-ui/icons/Edit';
import Save from '@material-ui/icons/Save';
import DeleteForever from '@material-ui/icons/DeleteForever';
//components
import { cafeApi } from '../../../services/Api';
import { SnackBarElement } from '../../../components/SnackBar';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function MenuPage(props) {

  //param
  const initialMenu = { title: '', content: '', harga: 0, gambar: 'default', id_kategori: null, id_menu: null };

  // state
  const [snack, setSnack] = useState({ open: false, variant: 'success', message: '', timeOut: 5000 });
  const [menu, setMenu] = useState([]);
  const [newMenu, setNewMenu] = useState(initialMenu);
  const [totalMenu, setTotalMenu] = useState(0);
  const [modal, setModal] = useState({ open: false, title: 'Hello Word' });
  const [dialog, setDialog] = useState({ open: false, title: 'Hello Word' });
  const [formValid, setFormValid] = useState(false)

  useEffect(() => {
    getMenu();
  }, [])

  const changeInput = name => event => {
    setNewMenu({ ...newMenu, [name]: event.target.value });
  };

  function getTotalMenu(data) {
    var count = 0;
    for (let i in data) {
      count += data[i].child.length
    }
    setTotalMenu(count)
  }

  function startSnackBar(param) {
    setSnack({
      ...snack,
      open: true,
      message: param.message,
      variant: param.variant
    });
    setTimeout(function () {
      setSnack({
        ...snack,
        open: false,
        variant: param.variant
      });
    }, snack.timeOut);
  }

  function getMenu() {
    cafeApi.get('/menu')
      .then(res => {
        setMenu(res.data.payload);
        getTotalMenu(res.data.payload);
      })
      .catch(err => {
        console.log(err)
      })
  }

  function changeCategory(event, index) {
    let state = [...menu];
    state[index] = { ...menu[index], judul: event.target.value };
    setMenu(state);
  }

  function handleFlag(index) {
    let state = [...menu];
    state[index] = {
      ...menu[index], save: menu[index].save === undefined ? true : !menu[index].save
    };
    setMenu(state)
  }

  function handleFlagChild(index, indexChild) {
    let state = [...menu];
    state[index].child[indexChild] = {
      ...menu[index].child[indexChild], save: menu[index].child[indexChild].save === undefined ? true : !menu[index].child[indexChild].save
    };
    setMenu(state)
  }

  function updateData(judul, id) {
    const payload = { id_kategori: id, judul: judul }
    if (id !== 'new') {
      cafeApi.put('/category', payload)
        .then(res => {
          console.log(res.data);
          startSnackBar({ message: res.data.payload, variant: 'success' })
        })
        .catch(err => {
          console.log(err);
          startSnackBar({ message: 'Edit Kategori Gagal', variant: 'error' })
          getMenu()
        })
    } else {
      cafeApi.post('/category', payload)
        .then(res => {
          startSnackBar({ message: res.data.payload, variant: 'success' })
          getMenu()
        })
        .catch(err => {
          console.log(err);
          startSnackBar({ message: 'Tambah Kategori Gagal', variant: 'error' })
          getMenu()
        })
    }
  }

  function fromValidation(data) {
    const keys = Object.keys(data);
    let valid = true
    for (let i in keys) {
      if (data[keys[i]] !== null && data[keys[i]].length !== undefined) {
        if (data[keys[i]].length < 3) {
          valid = false
          break;
        }
      }
    }
    return valid
  }

  function changeMenu() {
    let validator = fromValidation(newMenu);
    setFormValid(!validator)
    if (validator) {
      newMenu.id_menu === null ? cafeApi.post('/menu', newMenu) : cafeApi.put('/menu', newMenu)
        .then(res => {
          startSnackBar({ message: res.data.payload, variant: 'success' })
          getMenu()
          modalClose()
        })
        .catch(err => {
          console.log(err);
          startSnackBar({ message: 'Tambah Menu Gagal', variant: 'error' })
          getMenu()
        })
    }
  }

  function deleteMenu(data) {
    setDialog({
      ...dialog,
      open: false
    });
    var payload = { id_menu: data.id_menu, id_gambar: data.id_gambar };
    console.log(payload)
    cafeApi.delete('/menu', { data: payload })
      .then(res => {
        startSnackBar({ message: res.data.payload, variant: 'success' })
        getMenu()
      })
      .catch(err => {
        startSnackBar({ message: err.data.payload, variant: 'error' })
        getMenu()
      })
  }

  function modalOpen(payload, addMenu) {
    if (addMenu) {
      var data = initialMenu;
      data.id_kategori = payload.id;
      setNewMenu(data);
    } else {
      setNewMenu(payload.payload)
    }
    setModal({
      ...modal,
      open: true,
      title: payload.title
    });
  }

  function modalClose() {
    setModal({
      ...modal,
      open: false
    });
  }

  function getFiles(files) {
    if (files.file.size >= 302500) {
      setNewMenu({
        ...newMenu,
        gambar: { error: true, message: 'Ukuran Gambar Terlalu Besar' }
      });
      startSnackBar({ message: 'Ukuran Gambar Terlalu Besar', variant: 'error' })
    } else if (files.type.substring(0, 5) !== 'image') {
      setNewMenu({
        ...newMenu,
        gambar: { error: true, message: 'Format Gambar Tidak Benar' }
      });
      startSnackBar({ message: 'Format Gambar Tidak Benar', variant: 'error' })
    } else {
      setNewMenu({
        ...newMenu,
        gambar: files.base64
      });
    }
  }

  function modalDialog() {
    return (
      <div>
        <Dialog
          open={dialog.open}
          TransitionComponent={Transition}
          keepMounted
          // onClose={handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">Delete Confirmation</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Are You Sure Want To Delete This Data
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => {
                setDialog({
                  ...dialog,
                  open: false
                });
              }}
              color="primary"
            >
              No
            </Button>
            <Button
              onClick={() => {
                deleteMenu(dialog.payload)
              }}
              color="primary"
            >
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }

  function formDialog() {
    return (
      <Dialog open={modal.open} onClose={modalClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{modal.title}</DialogTitle>
        <DialogContent style={{ minWidth: 600 }} className='flex-center-row'>
          <img
            className={'img-preview'}
            src={newMenu.gambar === 'default' ? logo : newMenu.gambar}
            alt={'img-preview'}
          />
          <div className='padding-lg flex-center-column'>
            <p className='title-menu blue zero'>Upload Foto Menu</p>
            <p><small>( max 300kb )</small></p>
            {newMenu.gambar.error !== undefined ? (
              <p className='secondary' style={{ marginTop: 0 }}>{newMenu.gambar.message + ' !!!'}</p>
            ) : (
                <div />
              )}
            <FileBase64
              multiple={false}
              onDone={(event) => getFiles(event)}
            />
          </div>
        </DialogContent>
        <DialogContent>
          <div style={{ padding: '50px' }}>
            <TextField
              autoFocus
              variant='outlined'
              label="Judul"
              type="text"
              margin='normal'
              fullWidth
              placeholder={'Judul Menu'}
              error={formValid && newMenu.title.length < 3}
              value={newMenu.title}
              onChange={changeInput('title')}
            />
            <TextField
              variant='outlined'
              label="Deskripsi"
              type="text"
              margin='normal'
              fullWidth
              placeholder={'Diskripsi Menu'}
              error={formValid && newMenu.content.length < 3}
              value={newMenu.content}
              onChange={changeInput('content')}
            />
            <TextField
              variant='outlined'
              label="Harga"
              type="number"
              margin='normal'
              fullWidth
              placeholder={'Harga Makanan'}
              error={formValid && newMenu.harga.toString().length < 3}
              value={newMenu.harga}
              onChange={changeInput('harga')}
            />
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={modalClose} color="primary">
            Cancel
          </Button>
          <Button onClick={() => {
            changeMenu()
          }} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    )
  }

  return (
    <div>
      {SnackBarElement(snack)}
      {formDialog()}
      {modalDialog()}
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Paper className='paper-analityc secondary-bg' onClick={() => { console.log(newMenu) }}>
            <p className='zero center light'>Total Kategori</p>
            <div className='flex-center-column' style={{ height: 'calc(100% - 20px)' }}>
              <h1 className='light zero'>{menu.length}</h1>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className='paper-analityc secondary-bg'>
            <p className='zero center light'>Total Menu</p>
            <div className='flex-center-column' style={{ height: 'calc(100% - 20px)' }}>
              <h1 className='light zero'>{totalMenu}</h1>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className='table-container'>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell className='table-head' align="center">No</TableCell>
                  <TableCell className='table-head' align="left">Nama Kategori</TableCell>
                  <TableCell className='table-head' align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {menu.map((row, index) => (
                  <TableRow key={index.toString()}>
                    <TableCell align="center">{Number(index) + 1}</TableCell>
                    <TableCell align="left">
                      {row.save ? (
                        <input
                          type="text"
                          name="kategori"
                          value={row.judul}
                          className='table-input'
                          onChange={(event) => changeCategory(event, index)}
                        />
                      ) : (
                          <b>{row.judul}</b>
                        )}
                    </TableCell>
                    <TableCell align="center">
                      <Fab
                        color="primary"
                        size='small'
                        aria-label="add"
                        onClick={() => {
                          if(row.save && row.judul.length <= 3){
                            startSnackBar({ message: 'Judul Kategori Telalu Pendek', variant: 'error' })
                          } else {
                            handleFlag(index)
                          }
                          if (row.save) {
                            if (row.judul.length <= 3) {
                              startSnackBar({ message: 'Judul Kategori Telalu Pendek', variant: 'error' })
                            } else {
                              updateData(row.judul, row.id_kategori)
                            }
                          }
                        }}
                      >
                        {row.save ? (
                          <Save />
                        ) : (
                            <Edit />
                          )}
                      </Fab>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            <div className='space-md' />
            <div className='flex-center-row'>
              <Fab
                color="primary"
                size='small'
                aria-label="add"
                onClick={() => {
                  setMenu([...menu, { id_kategori: 'new', judul: '', save: true, child: [] }])
                }}
              >
                <LibraryAdd />
              </Fab>
              <p className='center'><b>Tabel Kategori</b></p>
            </div>
            <div className='space-md' />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className='paper-analityc flex-center-column secondary-bg' style={{ height: '50px' }}>
            <p className='light zero'>Tabel Menu Kedai Bosku</p>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          {menu.map((item, itemIndex) => (
            <Paper className='table-container' style={{ marginBottom: '20px' }} key={itemIndex.toString()}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell className='table-head' align="center">No</TableCell>
                    <TableCell className='table-head' align="left">Nama Menu</TableCell>
                    <TableCell className='table-head' align="left">Harga</TableCell>
                    <TableCell className='table-head' align="center">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {item.child.map((row, index) => (
                    <TableRow key={index.toString()}>
                      <TableCell align="center">{Number(index) + 1}</TableCell>
                      <TableCell align="left">
                        <b>{row.title}</b>
                      </TableCell>
                      <TableCell align="left">
                        <b>{row.harga}</b>
                      </TableCell>
                      <TableCell align="center">
                        <Fab
                          color="primary"
                          size='small'
                          aria-label="add"
                          onClick={() => {
                            modalOpen({ title: 'Edit Menu ' + item.judul, payload: row }, false);
                          }}
                        >
                          <Edit />
                        </Fab>
                        <Fab
                          color="primary"
                          size='small'
                          aria-label="add"
                          style={{ marginLeft: '10px' }}
                          onClick={() => {
                            setDialog({
                              ...dialog,
                              open: true,
                              payload: row
                            });
                          }}
                        >
                          <DeleteForever />
                        </Fab>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              <div className='space-md' />
              <div className='flex-center-row'>
                <Fab
                  color="primary"
                  size='small'
                  aria-label="add"
                  onClick={() => {
                    modalOpen({ title: 'Tambah Menu ' + item.judul, id: item.id_kategori }, true);
                  }}
                >
                  <LibraryAdd />
                </Fab>
                <p className='center'><b>Tabel {item.judul}</b></p>
              </div>
              <div className='space-md' />
            </Paper>
          ))}
        </Grid>
      </Grid>
    </div>
  );
}

export default withRouter(MenuPage)