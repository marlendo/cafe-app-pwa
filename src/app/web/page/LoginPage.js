import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { secureStorage } from '../../../services/SecureStorage';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//icon
//components
import { SaveLoad } from '../../../services/SafeLoad';
import { SnackBarElement } from '../../../components/SnackBar';
//api
import { cafeApi } from '../../../services/Api';
import { aesEncrypt } from '../../../services/SecureStorage';

function LoginPage(props) {
  const [login, setLogin] = useState({ user: '', pass: '', role: 'ADMIN' });
  const [snack, setSnack] = useState({ open: false, variant: 'success', message: '', timeOut: 3000 });

  const inputChange = flag => event => {
    setLogin({ ...login, [flag]: event.target.value });
  }

  function ceckLogin() {
    var payload = { email: login.user, pass: aesEncrypt(login.pass) };
    if (payload.email.length >= 3) {
      cafeApi.post('/login-bosku', payload)
        .then(res => {
          if (res.data.status === 200) {
            console.log(res.data.payload)
            secureStorage.setItem('session', res.data.payload);
            props.history.push({
              pathname: '/dashboard',
              state: {}
            })
          } else {
            startSnackBar({ message: res.data.payload, variant: 'error' })
          }
        })
        .catch(err => {
          startSnackBar({ message: 'Login Failed', variant: 'error' })
        })
    } else {
      startSnackBar({ message: 'Username & Password Salah !', variant: 'error' })
    }
  }

  function ceckSession() {
    const user = SaveLoad(secureStorage.getItem('session'));
    if (user.status) {
      if(user.role === 'ADMIN'){
        props.history.push({
          pathname: '/dashboard',
          state: {}
        })
      }
    }
  }

  function startSnackBar(param) {
    setSnack({
      ...snack,
      open: true,
      message: param.message,
      variant: param.variant
    });
    setTimeout(function () {
      setSnack({
        ...snack,
        open: false,
        variant: param.variant
      });
    }, snack.timeOut);
  }

  useEffect(() => {
    ceckSession();
  }, [])

  return (
    <div className='main-container blue-bg flex-center-row'>
      {SnackBarElement(snack)}
      <div className='aksesoris-login-1' />
      <div className='aksesoris-login-2' />
      <div className='aksesoris-login-3' onClick={() => { console.log(snack) }} />
      <Paper className='padding-lg' style={{ margin: '20px' }}>
        <div className='space' />
        <h1 className='title-cafe blue zero'>Kedai Bosku</h1>
        <div className='space' />
        <TextField
          label="Email"
          type='email'
          value={login.user}
          onChange={inputChange('user')}
          margin="normal"
          variant="outlined"
          fullWidth
          color='secondary'
        />
        <TextField
          label="Password"
          type="password"
          value={login.pass}
          onChange={inputChange('pass')}
          margin="normal"
          variant="outlined"
          fullWidth
        />
        <div className='space' />
        <Button
          variant="contained"
          color="primary"
          fullWidth
          onClick={() => {
            ceckLogin()
          }}>
          Login
      </Button>
      </Paper>
    </div>
  );
}

export default withRouter(LoginPage)